<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";

    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }
    
    if(isset($_POST['messageButton'])){
        if(!empty($_POST['message'])){

            echo " Message: " . $_POST['message'];
            echo " Id Conv: " . $_POST['idConv'];


            $message = $_POST['message'];

            // puis on entre les données en base de données :
            $insertion = $bdd->prepare('INSERT INTO messages(ChatID, UserID, message) VALUES(:ChatID, :UserID, :message)');
            $insertion->execute(array(
                'ChatID' => $_POST['idConv'],
                'UserID' => $_SESSION['ID'],
                'message' => $message
            ));
        }
        else{
            echo "Vous avez oublié de remplir un des champs !";
        }
    }
}

?>