<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";
    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }

    if(isset($_POST['importer'])){

        $image = $_FILES['image']['tmp_name'];
        $URL = $_FILES['image']['name'];
        $UserID = $_SESSION['ID'];

        $req = $bdd->prepare('INSERT INTO photo (URL, UserID) VALUES(:URL, :UserID)');
        
        $req->execute(array(
            'URL' => $URL,
            'UserID' => $UserID
        ));

        $reponse = $bdd->query('SELECT * FROM photo ORDER BY ID DESC');

        $PhotoID = $reponse->fetch()['ID'];//strval($reponse);
        $reponse->closeCursor();

        if(move_uploaded_file($image, "galerie/".$PhotoID.".jpg"))
            echo 'fichier ok';
        else
            echo 'téléchargement impossible';

        header('Location: gallerie.php');
    }

    if(isset($_POST['supprimer'])) 
    {
        if(isset($_POST['PhotoGalerieID'])){
            $PhotoID = $_POST['PhotoGalerieID'];
            $sql = "DELETE FROM photo WHERE ID = " . $PhotoID;
            $reponse = $bdd->query($sql);
            $path = "galerie/".$PhotoID.".jpg";
            unlink($path);
        }
    }

?>

    <!DOCTYPE html>

    <html>

    <?php include "html/head_begin.html"; ?>

    <title>Gallerie</title>

    <?php include "html/head_end.html"; ?>

    	<body>

    	<?php include "html/nav_connected.html"; ?>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <h1>Gallerie Photos</h1>
                <p>Ici, vous pouvez voir vos photos.</p>
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-md-3">
                    <div class="outfit">
                        
                        <h3><img src="../img/photo/album.png"> Mes Albums</h3>
                        <ul>

                            <li class="active-album">
                                <a href="#">
                                    <div class="scaledImageContainer"><img src="../img/1.jpg"></div>
                                    <p>Trip to Sweden</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="scaledImageContainer"><img src="../img/2.jpg"></div>
                                    <p>Italy 2015</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="scaledImageContainer"><img src="../img/3.jpg"></div>
                                    <p>Family 2014</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="scaledImageContainer"><img src="../img/4.jpg"></div>
                                    <p>The color run</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="scaledImageContainer"><img src="../img/5.jpg"></div>
                                    <p>Others</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div id="photo-div">
                        <h3><img src="../img/photo/6.png"> Photos de l'album</h3>
                        <form method="post" action="" enctype="multipart/form-data" id="float2">
                            <input type="file" class="btn btn-success" name="image">
                            <input type="submit" class="btn btn-success" value="Importer" name="importer" style="display: block">
                        </form>
                        <br>
                        <br>
                        <ul>
                            <?php
                            if($dossier = opendir('galerie')){
                                $compteur = 0;
                                while(false !== ($fichier = readdir($dossier))){
                                    $compteur++;

                                    if($compteur >= 3){
                                        $PhotoID = pathinfo($fichier, PATHINFO_FILENAME);
                                        $reponse = $bdd->query('SELECT * FROM photo WHERE ID = '.$PhotoID);
                                        $PhotoUserID = $reponse->fetch()['UserID'];
                                        $reponse->closeCursor();

                                        if($PhotoUserID == $_SESSION['ID']){
                                            echo '<li style="float: left; background-color: #fff;"><div>';
                                            echo '<div class="scaledImageContainer">';
                                            echo '<img src="galerie/'.$fichier.'" alt="photo">';
                                            echo '</div>';
                                            echo '<form method="post" action="" style="text-align: center;">
                                            <input type="hidden" class="btn btn-xs btn-danger" name="PhotoGalerieID" value="'.$PhotoID.'">
                                            <input class="btn btn-xs btn-danger" type="submit" value="supprimer" name="supprimer"/></form>';
                                            echo '</div></li>';
                                        }
                                    }
                                }
                                closedir($dossier);
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    	<?php include "html/copyright.html"; ?>

    	</body>

    </html>
<?php
}
?>