<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";

    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }


?>

    <!DOCTYPE html>

    <html>

    <?php include "html/head_begin.html"; ?>

    <title>Gallerie</title>

    <?php include "html/head_end.html"; ?>

    	<body>

    	<?php include "html/nav_connected.html"; ?>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="well" id="float1" style="position:fixed; width:20%">
                        <center><h3>Historique personnel</h3></center>
                        <div style="height:30px;"></div>
                        <img alt="slog" src="../img/slogan.png" style="width: 100%;">
                    </div>
                </div>
                <div class="col-md-6">

    <?php
    $sql = "SELECT * FROM publication WHERE publication.UserID = " . $_SESSION['ID'] . " ORDER BY DateHeure DESC";
    $reponse = $bdd->query($sql);

    while ($donnees = $reponse->fetch()) {
    ?>
                    <div class="well">
                        <p>


                            <?php
                            $result = glob("uploads/".$_SESSION['ID'].".jpg");

                            if($result){
                                $path = "uploads/".$_SESSION['ID'].".jpg";
                            }
                            else
                                $path="../img/friend.png";
                            ?>
                            <img alt="<?php echo $_SESSION['Prenom'] . " " . $_SESSION['Nom']; ?>" src="<?php echo $path?>" class="profile-pic">

                            <a href="#" class="black"><?php echo $_SESSION['Prenom'] . " " . $_SESSION['Nom']; ?></a> a publié quelque chose le: <a href="#" style="color:green;"> <?php echo $donnees['DateHeure']?></a>
                        </p>
                        <div class="well">
                        <?php if($donnees['PhotoID'] != NULL) { ?>
                            <p><img id="publication" alt="test" src="<?php echo "publications/".$donnees['PhotoID'].".jpg"?>" 
                            style="width:100%; 
                            <?php echo file_exists("publications/".$donnees['PhotoID'].".jpg") ? "" : " display:none;"; ?>
                            "></p>
                        <?php } ?>
                            <p><?php echo $donnees['Message'] ?></p>
                        </div>
                        <p>
                             <span  aria-hidden="true"><img src="../img/aime.png" style="width:35px;height:30px;"/></span>
                           <a href="#" class="black"><?php echo $donnees['NbLikes'] ?></a> personne(s) aiment cette publication.
                           <p></p>
                           <span  aria-hidden="true"><img src="../img/angry.png" style="width:35px;height:30px;"/></span>
                           <a href="#" class="black"><?php echo $donnees['Angry'] ?></a> personne(s) detestent cette publication.
                           <p></p>
                           <span aria-hidden="true"><img src="../img/reactions/laughing.png" style="width:35px;height:30px;"/></span>
                           <a href="#" class="black"><?php echo $donnees['Haha'] ?></a> personnes(s) rigolent de  cette publication.

                        </p>
                      
                    </div>

    <?php
    }

    $reponse->closeCursor();
    $bdd = null;
    ?>
                </div>
            </div><!-- 
            <a class="btn btn-success" href="index.php" role="button">Déconnexion</a> -->
        </div>

    	<?php include "html/copyright.html"; ?>

    </body>
    </html>

<?php
}
?>