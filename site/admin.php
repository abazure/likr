<?php

session_start();

if(!isset($_SESSION['Email'])){
  header('Location: index.php');
}
else
{
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "likr";

  try {
    $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "connected successfully";

  }
  catch(PDOException $e) {
    echo "connection failed: " . $e->getMessage();
  }

  if(isset($_POST['removePerson'])) {
    $req = $bdd->prepare('DELETE FROM utilisateur WHERE ID = :ID');
    $req->execute(array(
        'ID' => $_POST['person']
    ));
  }


  if(isset($_POST['formadmin'])) 
  {
        // On commence par récupérer les champs 
    if(isset($_POST['Nom']))
      $Nom=$_POST['Nom'];
    else
      $Nom="";

    if(isset($_POST['Prenom']))
      $Prenom=$_POST['Prenom'];
    else
      $Prenom="";

    if(isset($_POST['Email']))
      $Email=$_POST['Email'];
    else
      $Email="";

    if(isset($_POST['MDP']))
      $MDP=$_POST['MDP'];
    else
      $MDP="";

    if(isset($_POST['Sexe']))
      $Sexe=$_POST['Sexe'];
    else
      $Sexe="";

    if(isset($_POST['DateNaissance']))
      $DateNaissance=$_POST['DateNaissance'];
    else
      $DateNaissance="";

    if(!empty($_POST['Nom']) AND !empty($_POST['Prenom']) AND !empty($_POST['Email']) AND !empty($_POST['MDP']) AND !empty($_POST['Sexe'])AND !empty($_POST['DateNaissance'])) {

      if(filter_var($Email, FILTER_VALIDATE_EMAIL)) {
        $reqEmail = $bdd->prepare("SELECT * FROM utilisateur WHERE Email = ?");
        $reqEmail->execute(array($Email));
        $Emailexist = $reqEmail->rowCount();

        if($Emailexist == 0) {
          $req = $bdd->prepare('INSERT INTO utilisateur (Nom,Prenom,Email,MDP,Sexe,DateNaissance) VALUES(:Nom, :Prenom, :Email, :MDP, :Sexe, :DateNaissance)');
          $req->execute(array(
            'Nom' => $Nom,
            'Prenom' => $Prenom,
            'Email' => $Email,
            'MDP' => $MDP,
            'Sexe' => $Sexe,
            'DateNaissance' => $DateNaissance
            ));
          echo "Utilisateur Ajouté à la BDD";
        } 
        else {
          echo "Adresse mail existe déjà";
        }
      }
      else echo "mauvais format";
    }
    else{
      echo "<p style=\"color:red\">Veuillez remplir tous les champs</p>";
    }
  }

  if($_SESSION['Admin'] == 0){
    header('Location: profil.php');}











    ?>

    <!DOCTYPE html>

    <html>

    <?php include "html/head_begin.html"; ?>

    <title>Admin</title>

    <?php include "html/head_end.html"; ?>

    <body>

     <?php include "html/nav_connected.html"; ?>

     <!-- Main jumbotron for a primary marketing message or call to action -->
     <div class="jumbotron">
      <div class="container">
        <h1>Menu d'administration</h1>
        <p>Ici, vous pouvez ajouter et supprimer des utilisateurs.</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li role="presentation" class="active"><a href="#ajout" aria-controls="ajout" role="tab" data-toggle="tab">Ajouter un utilisateur</a></li>
          <li role="presentation"><a href="#suppression" aria-controls="suppression" role="tab" data-toggle="tab">Supprimer des utilisateurs</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <br>

          <div role="tabpanel" class="tab-pane active" id="ajout">
            <form method="POST" action="">
              <table>
               <tr>
                <td align="right">
                 <label for="Nom">Nom: &nbsp</label>
               </td>
               <td>
                 <input class="form-control" type="text" placeholder="Nom" id="Nom" name="Nom" value="<?php if(isset($Nom)) { echo $Nom; } ?>" />
               </td>
             </tr>
             <tr>
              <td align="right">
               <label for="Prenom">Prénom: &nbsp</label>
             </td>
             <td>
               <input class="form-control" type="text" placeholder="Prénom" id="Prenom" name="Prenom" value="<?php if(isset($Prenom)) { echo $Prenom; } ?>" />
             </td>
           </tr>
           <tr>
            <td align="right">
             <label for="Email">Email: &nbsp</label>
           </td>
           <td>
             <input class="form-control" type="email" placeholder="Email" id="Email" name="Email" value="<?php if(isset($Email)) { echo $Email; } ?>" />
           </td>
         </tr>
         <tr>
          <td align="right">
           <label for="MDP">Mot de passe: &nbsp</label>
         </td>
         <td>
           <input class="form-control" type="password" placeholder="*******" id="MDP" name="MDP" />
         </td>
       </tr>
       <tr>
        <td align="right">
         <label for="mdp2">Date de naissance: &nbsp</label>
       </td>
       <td>
         <input class="form-control" type="date" placeholder="YYYY-MM-DD" id="DateNaissance" name="DateNaissance" />
       </td>
     </tr>
     <tr>
      <td align="right">
       <label for="Sexe">Sexe: &nbsp</label>
     </td>
     <td>
       <div class="radio">
         <label><input type="radio" name="Sexe" value="H" checked>Homme</label>
       </div>
       <div class="radio">
         <label><input type="radio" name="Sexe" value="F">Femme</label>
       </div>
     </td>
   </tr>
   <tr>
     <td></td>
     <td align="center">
      <br />
      <input class="form-control" type="submit" name="formadmin" value="Valider"/>
    </td>
  </tr>
</table>
</form>

</div>


<div role="tabpanel" class="tab-pane" id="suppression">
  <table id="table1" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="table1_info" style="width: 100%;" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
          Id
        </th>
        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
          Prénom
        </th>
        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
          Nom
        </th>
        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
          Email
        </th>
        <th>
          Photo
        </th>
        <th>
          Profil
        </th>
        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
          Amis communs
        </th>
        <th>
          Supprimer
        </th>
      </tr>
    </thead>
    <tbody>
<?php
    $sql = "SELECT * FROM utilisateur WHERE utilisateur.ID <> ". $_SESSION['ID'];
    $reponse = $bdd->query($sql);

    while ($donnees = $reponse->fetch()) {
        if($donnees['ID'] != $_SESSION['ID']){
    ?>
                        <tr>
                            <td><?php echo $donnees['ID']; ?></td>
                            <td><?php echo $donnees['Prenom']; ?></td>
                            <td><?php echo $donnees['Nom']; ?></td>
                            <td><?php echo $donnees['Email']; ?></td>
                            
                            <?php
                            $result = glob("uploads/".$donnees['ID']."/profil.*");

                            if($result){
                                $imageFileType = pathinfo("uploads/".$donnees['ID']."/profil", PATHINFO_EXTENSION);
                                $path = "uploads/".$donnees['ID']."/profil.$imageFileType";
                            }
                            else
                                $path="../img/friend.png";
                            ?>
                            

                            <td><img alt="<?php echo $donnees['Prenom'] . " " . $donnees['Nom']; ?>" src="<?php echo $path?>" class="profile-pic"></td>
                            <td>
                                <form method="GET" action="profil.php">
                                    <input type="hidden" name="id" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" class="btn btn-success">Voir le profil</button>
                                </form>
                            </td>
                            
                            <td><button type="button" class="btn btn-info">Nb Amis communs</button></td>
                            
                            <td>
                                <form method="POST" action="">
                                    <input type="hidden" name="person" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" name="removePerson" value="remove"><img src="../img/no.png"/></button>
                                </form>
                            </td>
                        </tr>
<?php
    }
}
 ?>
    </tbody>
  </table>
</div>

</div>

</div>
</div>

<?php include "html/copyright.html"; ?>

</body>
</html>

<?php
}

?>