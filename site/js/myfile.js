$(document).ready(function() {
	$('#table1').DataTable();
	$('#table2').DataTable();
	$('#table3').DataTable();

	$('.outfit a').click(function(event) {
		event.preventDefault();
		$('.outfit li').removeClass('active-album');
		$(this).parent().addClass('active-album');
	});

    $('#slide').slideDown('fast/400/fast', function() {
        
    });

    $('.previewConversation').click(function(event) {
        event.preventDefault();
        var index = $(this).index() + 1;
        $('.previewConversation').removeClass('convSelected');
        $('.previewConversation:nth-child(' + index + ')').addClass('convSelected');
        $('.conversation').addClass('listeInactive');
        $('.conversation').removeClass('listeActive');
        $('.conversation:nth-child(' + index + ')').removeClass('listeInactive');
        $('.conversation:nth-child(' + index + ')').addClass('listeActive');

        var idConv = $('.conversation.listeActive').index() + 1;
        $('#idConv').val(idConv);

    });

    $('#messageForm').submit(function(e){
        // on empêche le bouton d'envoyer le formulaire
        e.preventDefault();

        var buttonMsg = $('#messageButton').val();
        var message = $('#messageInput').val();
        //alert(message);

        var idConv = $('.conversation.listeActive').index() + 1;
        console.log("Appui: " + idConv);

        if(message != "") {
            $.ajax({
                url : "traitement.php", // on donne l'URL du fichier de traitement
                type : "POST", // la requête est de type POST
                data : "messageButton=" + buttonMsg + "&message=" + message + "&idConv=" + idConv // et on envoie nos données
            });

           $('.listeMessages').prepend("<p>" + "Moi :" + message + "</p>").fadeIn('slow/400/fast', function() {
               
           });; // on ajoute le message dans la zone prévue
        }
    });


    function loadlink(){

        var idConv = $('.conversation.listeActive').index() + 1;
        console.log("Fetch: " + idConv);

        $('.listeMessages').load('charger.php' + '?idConv=' + idConv, function () {
            $(this).append();
        });
    }

    loadlink(); // This will run on page load
    setInterval(function(){
        loadlink() // this will run after every 5 seconds
    }, 1000);


    function getConvNum() {
        var thisConv = $('.conversation')

        for (var i = thisConv.length - 1; i >= 0; i--) {
            var elem = "h1";
            console.log(i);
            document.getElementsByClassName('conversation')[i].appendChild(document.createElement(elem));
        }

        $(thisConv).find(elem).css({
            position: 'absolute',
            left: '570px',
            top: '0'
        });

        $(thisConv).each(function(index, el) {
            $(this).find(elem).append(index + 1);
        });
    }

    getConvNum();

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgProfil')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$("#fileToUpload").change(function(){
    readURL(this);
});

var image = document.getElementById('imgProfil');
var cropper = new Cropper(image, {
  aspectRatio: 16 / 9,
  crop: function(e) {
    console.log(e.detail.x);
    console.log(e.detail.y);
    console.log(e.detail.width);
    console.log(e.detail.height);
    console.log(e.detail.rotate);
    console.log(e.detail.scaleX);
    console.log(e.detail.scaleY);
  }
});
