<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";
    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }

    $lastupdated = date('Y-m-d H:i:s');

    if(isset($_POST['addFriend'])){
        $req = $bdd->prepare('INSERT INTO ami (UserID1, UserID2, Accepted, WhoAsked, DateAmitie) VALUES(:UserID1, :UserID2, :Accepted, :WhoAsked, :DateAmitie)');
        $req->execute(array(
            'UserID1' => $_SESSION['ID'],
            'UserID2' => $_POST['futureFriendID'],
            'Accepted' => 0,
            'WhoAsked' => 1,
            'DateAmitie' => $lastupdated
        ));
    }
    
    if(isset($_POST['answer']) && $_POST['answer'] == "yes"){
        echo "Ta gueule";
        $req = $bdd->prepare('UPDATE ami SET Accepted = :Accepted, DateAmitie = :DateAmitie WHERE UserID1 = :UserID1 AND UserID2 = :UserID2');
        $req->execute(array(
            'Accepted' => 1,
            'DateAmitie' => $lastupdated,
            'UserID1' => $_POST['futureFriendID'],
            'UserID2' => $_SESSION['ID']
        ));
    }
    
    if(isset($_POST['removeFriend']) || (isset($_POST['answer']) && $_POST['answer'] == "no")){
        $req = $bdd->prepare('DELETE FROM ami WHERE UserID1 = :UserID1 AND UserID2 = :UserID2');
        $req->execute(array(
            'UserID1' => $_SESSION['ID'],
            'UserID2' => $_POST['futureFriendID']
        ));
        $req = $bdd->prepare('DELETE FROM ami WHERE UserID1 = :UserID1 AND UserID2 = :UserID2');
        $req->execute(array(
            'UserID1' => $_POST['futureFriendID'],
            'UserID2' => $_SESSION['ID']
        ));
    }


?>

    <!DOCTYPE html>

    <html>

    <?php include "html/head_begin.html"; ?>

    <title>Mur</title>

    <?php include "html/head_end.html"; ?>

    	<body>

    	<?php include "html/nav_connected.html"; ?>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
          <div class="container">
            <h1>Amis</h1>
            <p>Ici, vous pouvez voir vos amis, rechercher parmi eux et les trier. Vous pouvez également répondre aux invitations et ajouter nos recommandations d'amis.</p>
          </div>
        </div>

        <div class="container">
          <!-- Example row of columns -->
          <div class="row">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active"><a href="#tous" aria-controls="tous" role="tab" data-toggle="tab">Tous les amis</a></li>
                <li role="presentation"><a href="#invit" aria-controls="invit" role="tab" data-toggle="tab">Invitations reçues</a></li>
                <li role="presentation"><a href="#recom" aria-controls="recom" role="tab" data-toggle="tab">Recommandations</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <br>

                <div role="tabpanel" class="tab-pane active" id="tous">
                  
                  <table id="table1" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="table1_info" style="width: 100%;" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Id
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Prénom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Nom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Email
                            </th>
                            <th>
                                Photo
                            </th>
                            <th>
                                Profil
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Date d'ajout
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Amis communs
                            </th>
                            <th>
                                Retirer
                            </th>
                        </tr>
                    </thead>

    <?php
    $sql = "SELECT * FROM utilisateur WHERE utilisateur.ID IN (SELECT UserID2 FROM ami WHERE Accepted = '1' AND UserID1 = ". $_SESSION['ID'] .") OR utilisateur.ID IN (SELECT UserID1 FROM ami WHERE Accepted = '1' AND UserID2 = ". $_SESSION['ID'] .")";
    $reponse = $bdd->query($sql);

    while ($donnees = $reponse->fetch()) {
        if($donnees['ID'] != $_SESSION['ID']){
    ?>
    
                    <tbody>
                        <tr>
                            <td><?php echo $donnees['ID']; ?></td>
                            <td><?php echo $donnees['Prenom']; ?></td>
                            <td><?php echo $donnees['Nom']; ?></td>
                            <td><?php echo $donnees['Email']; ?></td>
                            
                            <?php
                            $result = glob("uploads/".$donnees['ID'].".jpg");

                            if($result){
                                $path = "uploads/".$donnees['ID'].".jpg";
                            }
                            else
                                $path="../img/friend.png";
                            ?>
                            

                            <td><img alt="<?php echo $donnees['Prenom'] . " " . $donnees['Nom']; ?>" src="<?php echo $path?>" class="profile-pic"></td>
                            <td>
                                <form method="GET" action="profil.php">
                                    <input type="hidden" name="id" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" class="btn btn-success">Voir le profil</button>
                                </form>
                            </td>
                            

                            <td>
                                <?php
                                $data = $bdd->query("SELECT DateAmitie FROM ami WHERE Accepted = '1' AND (UserID1 = ". $donnees['ID'] . ") OR (UserID2 = ". $donnees['ID'] . ")");
                                $dateAmitie = $data->fetch();
                                echo $dateAmitie['DateAmitie'];
                                ?>
                            </td>
                            <td><button type="button" class="btn btn-info">Nb Amis communs</button></td>
                            <td>
                                <form method="POST" action="">
                                    <input type="hidden" name="futureFriendID" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" name="removeFriend" value="remove"><img src="../img/remove-contact.png"/></button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
<?php
    }
}
 ?>
                  </table>
                </div>


                <div role="tabpanel" class="tab-pane" id="invit">
                  <table id="table2" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="table2_info" style="width: 100%;" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="sorting" tabindex="0" aria-controls="table2" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Id
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table2" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Prénom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table2" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Nom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table2" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Email
                            </th>
                            <th>
                                Photo
                            </th>
                            <th>
                                Profil
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table2" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Amis communs
                            </th>
                            <th>
                                Confirmer
                            </th>
                        </tr>
                    </thead>
                    <tbody>


    <?php
    $sql = "SELECT * FROM utilisateur WHERE utilisateur.ID IN (SELECT UserID1 FROM ami WHERE Accepted = '0' AND WhoAsked = '1' AND UserID2 = ". $_SESSION['ID'] .")";
    $reponse = $bdd->query($sql);

    while ($donnees = $reponse->fetch()) {
        if($donnees['ID'] != $_SESSION['ID']){
    ?>
                        <tr>
                            <td><?php echo $donnees['ID']; ?></td>
                            <td><?php echo $donnees['Prenom']; ?></td>
                            <td><?php echo $donnees['Nom']; ?></td>
                            <td><?php echo $donnees['Email']; ?></td>
                            
                            <?php
                            $result = glob("uploads/".$donnees['ID'].".jpg");

                            if($result){
                                $path = "uploads/".$donnees['ID'].".jpg";
                            }
                            else
                                $path="../img/friend.png";
                            ?>
                            

                            <td><img alt="<?php echo $donnees['Prenom'] . " " . $donnees['Nom']; ?>" src="<?php echo $path?>" class="profile-pic"></td>
                            <td>
                                <form method="GET" action="profil.php">
                                    <input type="hidden" name="id" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" class="btn btn-success">Voir le profil</button>
                                </form>
                            </td>
                            <td><button type="button" class="btn btn-info">Nb Amis communs</button></td>
                            <td>
                                <form method="POST" action="">
                                    <input type="hidden" name="futureFriendID" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" name="answer" value="no"><img src="../img/no.png"/></button>
                                    <button type="submit" name="answer" value="yes"><img src="../img/yes.png"/></button>
                                </form>
                            </td>
                        </tr>
<?php
    }
}
?>
                    </tbody>
                  </table>
                </div>

                <div role="tabpanel" class="tab-pane" id="recom">
                  <table id="table3" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="table3_info" style="width: 100%;" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Id
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Prénom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Nom
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Email
                            </th>
                            <th>
                                Photo
                            </th>
                            <th>
                                Profil
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                                Amis communs
                            </th>
                            <th>
                                Ajouter
                            </th>
                        </tr>
                    </thead>
                    <tbody>


    <?php
    $sql = "SELECT * FROM utilisateur WHERE utilisateur.ID NOT IN (SELECT UserID2 FROM ami WHERE UserID1 = ". $_SESSION['ID'] .") AND utilisateur.ID NOT IN (SELECT UserID1 FROM ami WHERE UserID2 = ". $_SESSION['ID'] .")";
    $reponse = $bdd->query($sql);

    while ($donnees = $reponse->fetch()) {
        if($donnees['ID'] != $_SESSION['ID']){
    ?>
                        <tr>
                            <td><?php echo $donnees['ID']; ?></td>
                            <td><?php echo $donnees['Prenom']; ?></td>
                            <td><?php echo $donnees['Nom']; ?></td>
                            <td><?php echo $donnees['Email']; ?></td>
                            
                            <?php
                            $result = glob("uploads/".$donnees['ID'].".jpg");

                            if($result){
                                $path = "uploads/".$donnees['ID'].".jpg";
                            }
                            else
                                $path="../img/friend.png";
                            ?>
                            
                            <td><img alt="<?php echo $donnees['Prenom'] . " " . $donnees['Nom']; ?>" src="<?php echo $path?>" class="profile-pic"></td>
                            <td>
                                <form method="GET" action="profil.php">
                                    <input type="hidden" name="id" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" class="btn btn-success">Voir le profil</button>
                                </form>
                            </td>
                            <td><button type="button" class="btn btn-info">Nb Amis communs</button></td>
                            <td>
                                <form method="POST" action="">
                                    <input type="hidden" name="futureFriendID" value="<?php echo $donnees['ID']; ?>">
                                    <button type="submit" name="addFriend" value="choose"><img src="../img/add-contact.png"/></button>
                                </form>
                            </td>
                        </tr>
<?php
    }
}
?>
                    </tbody>
                  </table>
                </div>
              </div>

          </div>
        </div>

    	<?php include "html/copyright.html"; ?>

      </body>
    </html>

<?php
}
?>