<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }

    // on récupère les messages ayant un id plus grand que celui donné
    $requete = $bdd->query('SELECT * FROM messages WHERE ChatID = ' . $_GET['idConv'] . ' ORDER BY ID DESC');
    //echo $_GET['idConv'];

    echo $_GET['idConv'];

    $messages = null;

    // on inscrit tous les nouveaux messages dans une variable
    while($donnees = $requete->fetch()){
        $user = $bdd->query('SELECT * FROM utilisateur WHERE ID = ' . $donnees['UserID']);
        $userData = $user->fetch();

        $messages .= "<p id=\"" . $donnees['ID'] . "\" ";
        if($userData['ID'] == $_SESSION['ID'])
            $messages .= "style=\"color: green;\"";
        else
            $messages .= "style=\"color: red;\"";
        $messages .= ">" . $userData['Prenom'] . $userData['Nom'] ." : " . $donnees['Message'] . "</p>";
    }
    echo $messages; // enfin, on retourne les messages à notre script JS

}
?>
