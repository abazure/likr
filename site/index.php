<?php

session_start();

if(isset($_SESSION['Email'])){
    header('Location: profil.php');
}
else{

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "likr";

	try {
	    $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	    // set the PDO error mode to exception
	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    echo "connected successfully";

	}
	catch(PDOException $e) {
	    echo "connection failed: " . $e->getMessage();
	}

	$rempli = 0;


	if(isset($_POST['formconnexion'])) 
	{
		if(isset($_POST['Email']))
			$Email=$_POST['Email'];
		else
			$Email="";

		if(isset($_POST['MDP']))
			$MDP=$_POST['MDP'];
		else
			$MDP="";

		// Vérification des identifiants
		$req = $bdd->prepare('SELECT * FROM utilisateur WHERE Email = :Email AND MDP = :MDP');
		$req->execute(array(
		    'Email' => $Email,
		    'MDP' => $MDP));
		$resultat = $req->fetch();

		if (!$resultat){
		    //echo 'Mauvais identifiant ou mot de passe !';
		    $rempli = 4;
		}
		else{
		    //session_start();
		    $_SESSION['ID'] = $resultat['ID'];
		    $_SESSION['Nom'] = $resultat['Nom'];
		    $_SESSION['Prenom'] = $resultat['Prenom'];
		    $_SESSION['DateNaissance'] = $resultat['DateNaissance'];
		    $_SESSION['Sexe'] = $resultat['Sexe'];
		    $_SESSION['Admin'] = $resultat['Admin'];
		    $_SESSION['Email'] = $resultat['Email'];
		    $_SESSION['Tel'] = $resultat['Tel'];
            $_SESSION['Pays'] = $resultat['Pays'];
            $_SESSION['Emploi'] = $resultat['Emploi'];
            $_SESSION['Scolarite'] = $resultat['Scolarite'];
            $_SESSION['Statut'] = $resultat['Statut'];

		    $rempli = 5;
    		header('Location: profil.php');
		}
	}


	if(isset($_POST['forminscription'])) 
	{
		// On commence par récupérer les champs 
		if(isset($_POST['Nom']))
			$Nom=$_POST['Nom'];
		else
			$Nom="";

		if(isset($_POST['Prenom']))
			$Prenom=$_POST['Prenom'];
		else
			$Prenom="";

		if(isset($_POST['Email']))
			$Email=$_POST['Email'];
		else
			$Email="";

		if(isset($_POST['MDP']))
			$MDP=$_POST['MDP'];
		else
			$MDP="";

		if(isset($_POST['Sexe']))
			$Sexe=$_POST['Sexe'];
		else
			$Sexe="";

		if(isset($_POST['DateNaissance']))
			$DateNaissance=$_POST['DateNaissance'];
		else
			$DateNaissance="";

		if(!empty($_POST['Nom']) AND !empty($_POST['Prenom']) AND !empty($_POST['Email']) AND !empty($_POST['MDP']) AND !empty($_POST['Sexe'])AND !empty($_POST['DateNaissance'])) {

			if(filter_var($Email, FILTER_VALIDATE_EMAIL)) {
				$reqEmail = $bdd->prepare("SELECT * FROM utilisateur WHERE Email = ?");
				$reqEmail->execute(array($Email));
				$Emailexist = $reqEmail->rowCount();

				if($Emailexist == 0) {
					$req = $bdd->prepare('INSERT INTO utilisateur (Nom,Prenom,Email,MDP,Sexe,DateNaissance) VALUES(:Nom, :Prenom, :Email, :MDP, :Sexe, :DateNaissance)');
					$req->execute(array(
						'Nom' => $Nom,
						'Prenom' => $Prenom,
						'Email' => $Email,
						'MDP' => $MDP,
						'Sexe' => $Sexe,
						'DateNaissance' => $DateNaissance
						));
					$rempli = 3;
				} 
				else {
					$rempli = 2;
				}
			}
		}
		else{
			$rempli = 1;
		}
	}
	?>

	<!DOCTYPE html>

	<html>

	<?php include "html/head_begin.html"; ?>

	<title>Accueil</title>

	<?php include "html/head_end.html"; ?>

	<body>

		<?php include "html/nav_disconnected.html"; ?>
		<div class="jumbotron">
			<?php include "html/inscription.html"; ?>


	<?php
		switch ($rempli) {
			case 0:
				break;
			
			case 1:
				echo "<p style=\"color:red\">Veuillez remplir tous les champs</p>";
				break;
			
			case 2:
				echo "Adresse mail déjà utilisée !";
				break;
			
			case 3:
				echo "Utilisateur ajouté à la BDD !";
				break;
				
			case 4:
				echo "Mauvais Email ou Mot de Passe";
				break;

			case 5:
				echo "Connecté avec succès";
				break;

			default:
				echo "Autre cas ?";
				break;
		}
		echo $rempli;
	?>

		</div>
		<?php include "html/copyright.html"; ?>

	</body>

	</html>
<?php
}
?>