<?php

session_start();

if(!isset($_SESSION['Email'])){
    header('Location: index.php');
}
else
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "likr";

    try {
        $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";

    }
    catch(PDOException $e) {
        echo "connection failed: " . $e->getMessage();
    }

    $creation = date('Y-m-d H:i:s');

    if(isset($_POST['newChatButton'])){
        if(isset($_POST['groupName'])){
            // Nouveau Chat
            $req = $bdd->prepare('INSERT INTO chat (Nom, DateCreation) VALUES(:Nom, :DateCreation)');
            $req->execute(array(
                'Nom' => $_POST['groupName'],
                'DateCreation' => $creation
            ));

            $req = $bdd->query("SELECT * FROM chat WHERE Nom = " . $_POST['groupName'] /*. " AND DateCreation = " . $creation*/);
            $reponse = $req->fetch();

            // Nouveau Participant (moi)
            $req = $bdd->prepare('INSERT INTO participants (ChatID, UserID) VALUES(:ChatID, :UserID)');
            $req->execute(array(
                'ChatID' => $reponse['ID'],
                'UserID' => $_SESSION['ID']
            ));

        }
    }

?>

    <!DOCTYPE html>

    <html>

    <?php include "html/head_begin.html"; ?>

    <title>Messenger</title>

    <?php include "html/head_end.html"; ?>

    	<body>

    	<?php include "html/nav_connected.html"; ?>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <h1>Messenger</h1><!-- 
                <p>Likez jusqu'aux messages avec Likr.</p> -->
            </div>
        </div>

        <form method="POST" action="" id="newChatForm">
            <input type="text" class="form-control" name="groupName" id="groupName" placeholder="Nom du groupe...">
            <button id="newChatButton" type="submit" class="btn btn-primary" name="newChatButton" value="envoi">Créer un chat</button>
        </form>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div id="listeConversations">
<?php
    $first = 1;
    $requeteChat = $bdd->query('SELECT * FROM chat ORDER BY ID');
    while($donneesChat = $requeteChat->fetch()){
        if($first == 1){
            echo "<div class=\"previewConversation convSelected\">";
        }
        else {
            echo "<div class=\"previewConversation\">";
        }
        
        $first = 0;

        echo "<p id=\"conv" . $donneesChat['ID'] . "\">". $donneesChat['ID']. ". " .$donneesChat['Nom'] . " <br>Date de création : " . $donneesChat['DateCreation'] . "</p>";
        echo "</div>";
    }

    $requeteChat->closeCursor();

    $first = 1;
?>
                </div>
                <div id="contenuConversation">
<?php

    $requeteChat = $bdd->query('SELECT * FROM chat ORDER BY ID');
    while($donneesChat = $requeteChat->fetch()){

?>
                    <div class="conversation">
<?php

        if($first == 1){
            echo "<div class=\"listeMessages listeActive\">";
        }
        else {
            echo "<div class=\"listeMessages\">";
        }
                  
        $first = 0;

        echo $donneesChat['ID'];

    $requeteMessages = $bdd->query('SELECT * FROM messages WHERE ChatID ='. $donneesChat['ID'] .' ORDER BY ID DESC LIMIT 0,10');

    while($donneesMessages = $requeteMessages->fetch()){

        echo "<p id=\"" . $donneesMessages['ID'] . "\">" . $donneesMessages['UserID'] . " : " . $donneesMessages['Message'] . "</p>";
    }
    ?>
                        </div>
                        <div class="listeParticipants">

    <?php
/*        $sql = "SELECT * FROM utilisateur WHERE utilisateur.ID IN (SELECT UserID2 FROM ami WHERE Accepted = '1' AND UserID1 = ". $_SESSION['ID'] .") OR utilisateur.ID IN (SELECT UserID1 FROM ami WHERE Accepted = '1' AND UserID2 = ". $_SESSION['ID'] .")";
        $requeteAmis = $bdd->query(sql);
        while($donneesAmis = $requeteAmis->fetch()){
            echo "<button type=\"submit\" id=\"" . $donneesAmis['ID'] . "\">" . $donneesAmis['ID'] . ". " . $donneesAmis['Prenom'] . $donneesAmis['Nom'] . "</button>";
        }
        $requeteAmis->closeCursor();*/
    ?>

                        </div>
                     </div>

    <?php
}
    $requeteMessages->closeCursor();
?>

                    <form method="POST" action="traitement.php" class="sendMessage" id="messageForm">
                        <input type="hidden" id="idConv" name="idConv" value="">
                        <input type="text" class="form-control" name="message" id="messageInput" placeholder="Ecrivez un message ici...">
                        <button id="messageButton" type="submit" class="btn btn-default" name="messageButton">Envoyer un message</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?php include "html/copyright.html"; ?>

	</body>

</html>

<?php
}
?>